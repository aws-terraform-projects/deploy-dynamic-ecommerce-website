# Configure AWS provider
provider "aws" {
  region = "us-east-1"
  profile = "terraform-user"
}

# Stores terraform state files in S3
terraform {
  backend "s3" {
    bucket = "dev001-terraform-remote-state"
    key    = "terraform.tfstate.dev"
    region = "us-east-1"
    profile = "terraform-user"
  }
}